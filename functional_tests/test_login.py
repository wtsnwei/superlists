import re, os, poplib, time, sys
from django.core import mail
from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest


SUBJECT = 'Your login link for Superlists'

class LoginTest(FunctionalTest):

    def wait_for_email(self, test_email, subject):
        # if not self.staging_server:
        #     email = mail.outbox[0]
        #     self.assertIn(test_email, email.to)
        #     self.assertEqual(email.subject, subject)
        #     return email.body

        for arg in sys.argv:
            if 'liveserver' in arg:
                email_id = None
                start = time.time()
                inbox = poplib.POP3_SSL('pop.163.com')
                try:
                    inbox.user(test_email)
                    inbox.pass_(os.environ['EMAIL_PASSWORD'])
                    while time.time() - start < 60:
                        # get 10 newest messages
                        count, _ = inbox.stat()
                        for i in reversed(range(max(1, count - 10), count + 1)):
                            print('getting msg', i)
                            _, lines, __ = inbox.retr(i)
                            lines = [l.decode('utf8') for l in lines]
                            if 'Subject: {}'.format(subject) in lines:
                                email_id = i
                                body = '\n'.join(lines)
                                return body
                        time.sleep(1)
                finally:
                    if email_id:
                        inbox.dele(email_id)
                    inbox.quit()

        email = mail.outbox[0]
        self.assertIn(test_email, email.to)
        self.assertEqual(email.subject, subject)
        return email.body

    def test_cant_get_email_link_to_log_in(self):
        # if self.staging_server:
        #     test_email = 'wtsnwei@163.com'
        # else:
        #     test_email = 'edith@example.com'
        for arg in sys.argv:
            if 'liveserver' in arg:
                test_email = 'wtsnwei@163.com'
            else:
                test_email = 'edith@example.com'

        # 伊迪斯访问这个很棒的 superlists 网站
        # 第一次注意到"Sign in"链接
        self.browser.get(self.server_url)
        self.browser.find_element_by_name('email').send_keys(test_email)
        self.browser.find_element_by_name('email').send_keys(Keys.ENTER)

        # 出现一条消息，告诉她已发送电子邮件
        self.wait_for(lambda: self.assertIn(
            'Check your email',
            self.browser.find_element_by_tag_name('body').text
        ))

        # 伊迪斯检查她的电子邮件地址,发现了一条信息
        body = self.wait_for_email(test_email, SUBJECT)

        # 有一个URL链接(It ha a url link in it)
        self.assertIn('Use this link to log in', body)
        url_search = re.search(r'http://.+/.+$', body)
        if not url_search:
            self.fail('Could not find url in email body:\n%s' % body)
        url = url_search.group(0)
        if '@' in self.server_url:
            self.server_url = self.server_url[0:7] + self.server_url[12:]
        self.assertIn(self.server_url, url)

        # 她点击链接
        self.browser.get(url)

        # 她发现自己已经登陆
        self.wait_to_be_logged_in(email=test_email)

        # 现在她要退出
        self.browser.find_element_by_link_text('Log out').click()

        # 她退出了
        self.wait_to_be_logged_out(email=test_email)
