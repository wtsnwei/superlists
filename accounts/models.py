from django.db import models
from django.contrib import auth
from django.utils import timezone
from django.contrib.auth.models import UserManager 
import uuid


class User(models.Model):
	email = models.EmailField(primary_key=True)
	last_login = models.DateTimeField(default=timezone.now)
	REQUIRED_FIELDS = ()
	USERNAME_FIELD = 'email'

	def is_authenticated(self):
		return True


class Token(models.Model):
	email = models.EmailField()
	uid = models.CharField(default=uuid.uuid4, max_length=40)
